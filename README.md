# OpenML dataset: Diabetes130US

https://www.openml.org/d/4541

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Attila Reiss, Department Augmented Vision, DFKI, Germany, "attila.reiss '@' dfki.de  
**Date**: August 2012.  
**Source**: UCI  
**Please cite**: Beata Strack, Jonathan P. DeShazo, Chris Gennings, Juan L. Olmo, Sebastian Ventura, Krzysztof J. Cios, and John N. Clore, &ldquo; Impact of HbA1c Measurement on Hospital Readmission Rates: Analysis of 70,000 Clinical Database Patient Records,&rdquo; BioMed Research International, vol. 2014, Article ID 781670, 11 pages, 2014.  

This data has been prepared to analyze factors related to readmission as well as other outcomes pertaining to patients with diabetes.

**Source**  
The data are submitted on behalf of the Center for Clinical and Translational Research, Virginia Commonwealth University, a recipient of NIH CTSA grant UL1 TR00058 and a recipient of the CERNER data. John Clore (jclore '@' vcu.edu), Krzysztof J. Cios (kcios '@' vcu.edu), Jon DeShazo (jpdeshazo '@' vcu.edu), and Beata Strack (strackb '@' vcu.edu). This data is a de-identified abstract of the Health Facts database (Cerner Corporation, Kansas City, MO).

**Data Set Information**  
The dataset represents 10 years (1999-2008) of clinical care at 130 US hospitals and integrated delivery networks. It includes over 50 features representing patient and hospital outcomes. Information was extracted from the database for encounters that satisfied the following criteria:  
(1) It is an inpatient encounter (a hospital admission).  
(2) It is a diabetic encounter, that is, one during which any kind of diabetes was entered to the system as a diagnosis.  
(3) The length of stay was at least 1 day and at most 14 days.  
(4) Laboratory tests were performed during the encounter.  
(5) Medications were administered during the encounter.  
The data contains such attributes as patient number, race, gender, age, admission type, time in hospital, medical specialty of admitting physician, number of lab test performed, HbA1c test result, diagnosis, number of medication, diabetic medications, number of outpatient, inpatient, and emergency visits in the year before the hospitalization, etc.

**Attribute Information**  
Detailed description of all the attributes is provided in Table 1 of the paper.  

**Relevant Papers**  
Beata Strack, Jonathan P. DeShazo, Chris Gennings, Juan L. Olmo, Sebastian Ventura, Krzysztof J. Cios, and John N. Clore, &ldquo;Impact of HbA1c Measurement on Hospital Readmission Rates: Analysis of 70,000 Clinical Database Patient Records,&rdquo; BioMed Research International, vol. 2014, Article ID 781670, 11 pages, 2014.

[Web Link](https://www.hindawi.com/journals/bmri/2014/781670/)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4541) of an [OpenML dataset](https://www.openml.org/d/4541). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4541/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4541/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4541/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

